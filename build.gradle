// Global gradle configuration. This configuration will be used for all sub-projects.
apply from: './configuration.gradle'
apply from: './repositories.gradle'
apply from: './dependencies.gradle'

/**
 * Global build script configuration ===============================================================
 */
buildscript {
    apply from: './configuration.gradle'
    apply from: './repositories.gradle'
    apply from: './dependencies.gradle'

    repositories {
        google()
        jcenter()
        maven { url repos.fabric }
    }

    dependencies {
        classpath 'com.android.tools.build:gradle:3.6.1'
        classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:${versions.kotlin}"
        classpath 'com.vanniktech:gradle-android-junit-jacoco-plugin:0.15.0'
        classpath 'com.jfrog.bintray.gradle:gradle-bintray-plugin:1.8.4'
    }
}

/**
 * All projects global configuration ===============================================================
 */
allprojects {
    repositories {
        google()
        jcenter()
        maven { url repos.fabric }
        maven { url repos.bintray.universum.studios.android }
        mavenLocal()
        maven { url repos.project.artifacts }
    }

    // Disable pre-dexing of libraries when running builds in CI environment.
    if (config.ci.EXECUTING == true) {
        project.plugins.whenPluginAdded { plugin ->
            if (plugin.class.name == "com.android.build.gradle.AppPlugin") {
                logger.quiet("Disabled pre-dexing for module :${project.name}")
                project.android.dexOptions.preDexLibraries = false
            } else if (plugin.class.name == "com.android.build.gradle.LibraryPlugin") {
                logger.quiet("Disabled pre-dexing for library module :${project.name}")
                project.android.dexOptions.preDexLibraries = false
            }
        }
    }
}

/**
 * All projects global tasks =======================================================================
 */

/**
 * Task that cleans build directory of the root project.
 */
task clean(type: Delete) {
    delete rootProject.buildDir
}

/**
 * Task that assembles release variant of all library modules.
 */
task assembleLibrary() {
    group 'build'
    description 'Assembles release variant of all library modules.'
    subprojects.findAll { if (it.name.startsWith("library")) dependsOn ":${it.name}:assembleRelease" }
}

/**
 * Task that deploys (uploads) all library modules into Maven local repository.
 */
task deployToMavenLocal() {
    group 'deploy'
    description 'Deploys artifacts for all library modules to the local Maven repository.'
    subprojects.findAll { if (it.name.startsWith("library")) dependsOn ":${it.name}:publishToMavenLocal" }
}

/**
 * Task that deploys (uploads) all library modules up to the Bintray repository.
 */
task deployToBintray() {
    group 'deploy'
    description 'Deploys artifacts for all library modules up to the Bintray repository.'
    subprojects.findAll { if (it.name.startsWith("library")) dependsOn ":${it.name}:bintrayUpload" }
}

/**
 * Task that updates library artifacts directory.
 */
task updateArtifacts() {
    subprojects.findAll { if (it.name.startsWith("library")) dependsOn ":${it.name}:updateArtifacts" }
    doLast {
        if (file(repos.project.artifacts).exists()) {
            exec {
                commandLine 'git', 'add', repos.project.artifacts, '-A'
            }
        }
    }
}