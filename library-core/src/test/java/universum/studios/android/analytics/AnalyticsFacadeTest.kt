/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoInteractions
import org.mockito.Mockito.verifyNoMoreInteractions
import universum.studios.android.test.AndroidTestCase

/**
 * @author Martin Albedinsky
 */
class AnalyticsFacadeTest : AndroidTestCase() {

    @Test fun testSilentDestination() {
        // Act + Assert:
        assertThat(AnalyticsFacade.SILENT_DESTINATION, `is`(notNullValue()))
        assertThat(AnalyticsFacade.SILENT_DESTINATION, `is`(SilentDestination))
    }

    @Test fun testFindDestination() {
        // Arrange:
        val mockDestinationFirst = mock(AnalyticsDestination::class.java)
        `when`(mockDestinationFirst.getName()).thenReturn("FIRST")
        val mockDestinationSecond = mock(AnalyticsDestination::class.java)
        `when`(mockDestinationSecond.getName()).thenReturn("SECOND")
        val analytics = TestAnalytics.Builder()
                .addDestination(mockDestinationFirst)
                .addDestination(mockDestinationSecond)
                .build()
        // Act + Assert:
        assertThat(analytics.findDestination("FIRST"), `is`(mockDestinationFirst))
        assertThat(analytics.findDestination("SECOND"), `is`(mockDestinationSecond))
    }

    @Test fun testFindDestinationThatIsNotRegistered() {
        // Arrange:
        val mockDestinationFirst = mock(AnalyticsDestination::class.java)
        `when`(mockDestinationFirst.getName()).thenReturn("FIRST")
        val mockDestinationSecond = mock(AnalyticsDestination::class.java)
        `when`(mockDestinationSecond.getName()).thenReturn("SECOND")
        val analytics = TestAnalytics.Builder()
                .addDestination(mockDestinationFirst)
                .addDestination(mockDestinationSecond)
                .build()
        // Act + Assert:
        assertThat(analytics.findDestination("THIRD"), `is`<AnalyticsDestination>(AnalyticsFacade.SILENT_DESTINATION))
    }

    @Test fun testLogEvent() {
        // Arrange:
        val mockDestinationFirst = mock(AnalyticsDestination::class.java)
        `when`(mockDestinationFirst.getName()).thenReturn("FIRST")
        val mockDestinationSecond = mock(AnalyticsDestination::class.java)
        `when`(mockDestinationSecond.getName()).thenReturn("SECOND")
        val analytics = TestAnalytics.Builder()
                .addDestination(mockDestinationFirst)
                .addDestination(mockDestinationSecond)
                .build()
        Mockito.clearInvocations(mockDestinationFirst, mockDestinationSecond)
        val mockEvent = mock(AnalyticsEvent::class.java)
        `when`(mockEvent.getDestinations()).thenReturn(hashSetOf("FIRST"))
        // Act:
        analytics.logEvent(mockEvent)
        // Assert:
        verify(mockDestinationFirst).logEvent(mockEvent)
        verify(mockEvent).getDestinations()
        verifyNoMoreInteractions(mockDestinationFirst, mockEvent)
        verifyNoInteractions(mockDestinationSecond)
    }

    @Test fun testLogFailure() {
        // Arrange:
        val mockDestinationFirst = mock(AnalyticsDestination::class.java)
        `when`(mockDestinationFirst.getName()).thenReturn("FIRST")
        val mockDestinationSecond = mock(AnalyticsDestination::class.java)
        `when`(mockDestinationSecond.getName()).thenReturn("SECOND")
        val analytics = TestAnalytics.Builder()
                .addDestination(mockDestinationFirst)
                .addDestination(mockDestinationSecond)
                .build()
        Mockito.clearInvocations(mockDestinationFirst, mockDestinationSecond)
        val mockFailure = mock(AnalyticsFailure::class.java)
        `when`(mockFailure.getDestinations()).thenReturn(hashSetOf("SECOND"))
        // Act:
        analytics.logFailure(mockFailure)
        // Assert:
        verify(mockDestinationSecond).logFailure(mockFailure)
        verify(mockFailure).getDestinations()
        verifyNoMoreInteractions(mockDestinationSecond, mockFailure)
        verifyNoInteractions(mockDestinationFirst)
    }

    private class TestAnalytics(builder: Builder) : AnalyticsFacade(builder) {

        class Builder : AnalyticsFacade.FacadeBuilder<Builder>() {
            override val self = this
            override fun build() = TestAnalytics(this)
        }
    }
}