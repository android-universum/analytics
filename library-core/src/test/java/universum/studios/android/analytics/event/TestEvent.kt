/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics.event

import universum.studios.android.analytics.AnalyticsEvent
import universum.studios.android.analytics.ConsoleDestination
import java.io.Serializable

/**
 * @author Martin Albedinsky
 */
class TestEvent : AnalyticsEvent {

    override fun getDestinations() = hashSetOf(ConsoleDestination.NAME)
    override fun getType() = "TestEvent"
    override fun getAttributes(): Map<String, Serializable> = hashMapOf()
    override fun toString() = "${getType()}{destinations: ${getDestinations()}, attributes: {}}"
}