/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verifyNoInteractions
import universum.studios.android.test.TestCase

/**
 * @author Martin Albedinsky
 */
class AnalyticsTest : TestCase() {

    @Test fun testEmpty() {
        // Act + Assert:
        assertThat(Analytics.empty(), `is`(notNullValue()))
        assertThat(Analytics.empty(), `is`(Analytics.empty()))
    }

    @Test fun testEmptyInstance() {
        // Act + Assert:
        assertThat(Analytics.EMPTY, `is`(notNullValue()))
    }

    @Test fun testEmptyInstanceLogEvent() {
        // Arrange:
        val mockEvent = mock(AnalyticsEvent::class.java)
        // Act:
        Analytics.EMPTY.logEvent(mockEvent)
        // Assert:
        verifyNoInteractions(mockEvent)
    }

    @Test fun testEmptyInstanceLogFailure() {
        // Arrange:
        val mockFailure = mock(AnalyticsFailure::class.java)
        // Act:
        Analytics.EMPTY.logFailure(mockFailure)
        // Assert:
        verifyNoInteractions(mockFailure)
    }

    @Suppress("unused")
    class TestAnalytics : Analytics {

        override fun logEvent(event: AnalyticsEvent) {}

        override fun logFailure(failure: AnalyticsFailure) {}
    }
}