/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoInteractions
import org.mockito.Mockito.verifyNoMoreInteractions
import universum.studios.android.analytics.event.TestEvent
import universum.studios.android.analytics.event.TestFailure
import universum.studios.android.logging.Logger
import universum.studios.android.test.TestCase

/**
 * @author Martin Albedinsky
 */
class ConsoleDestinationTest : TestCase() {

    @Test fun testName() {
        // Act + Assert:
        assertThat(ConsoleDestination.NAME, `is`("CONSOLE"))
    }

    @Test fun testCreate() {
        // Arrange:
        val mockLogger = mock(Logger::class.java)
        // Act:
        val destination = ConsoleDestination.create(mockLogger)
        // Assert:
        assertThat(destination, `is`(notNullValue()))
        assertThat(destination.getName(), `is`(ConsoleDestination.NAME))
        assertThat(destination, `is`(not(ConsoleDestination.create(mockLogger))))
        verifyNoInteractions(mockLogger)
    }

    @Test fun testLogEvent() {
        // Arrange:
        val mockLogger = mock(Logger::class.java)
        val destination = ConsoleDestination.create(mockLogger)
        val event = TestEvent()
        // Act:
        destination.logEvent(event)
        // Assert:
        verify(mockLogger).i(ConsoleDestination.TAG, "Received event => $event")
        verifyNoMoreInteractions(mockLogger)
    }

    @Test fun testLogFailure() {
        // Arrange:
        val mockLogger = mock(Logger::class.java)
        val destination = ConsoleDestination.create(mockLogger)
        val failure = TestFailure()
        // Act:
        destination.logFailure(failure)
        // Assert:
        verify(mockLogger).w(ConsoleDestination.TAG, "Received failure => $failure")
        verifyNoMoreInteractions(mockLogger)
    }
}