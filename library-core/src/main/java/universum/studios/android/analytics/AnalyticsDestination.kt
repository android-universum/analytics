/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics

import androidx.annotation.NonNull

/**
 * Destination for [AnalyticsEvents][AnalyticsEvent] and [AnalyticsFailures][AnalyticsFailure], that
 * is, a place where desired events may be logged/sent.
 *
 * Each destination is expected to represent a single analytics product, commonly cloud based, to
 * which either analytics events or analytics failures may be sent.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
interface AnalyticsDestination {

    /**
     * Returns the unique name of this analytics destination.
     *
     * @return Name by which may be this destination uniquely identified.
     */
    @NonNull fun getName(): String

    /**
     * Logs the given [event] into this destination.
     *
     * @param event The desired event to be logged.
     *
     * @see logFailure
     */
    fun logEvent(@NonNull event: AnalyticsEvent)

    /**
     * Logs the given [failure] into this destination.
     *
     * @param failure The desired failure to be logged.
     *
     * @see logEvent
     */
    fun logFailure(@NonNull failure: AnalyticsFailure)
}