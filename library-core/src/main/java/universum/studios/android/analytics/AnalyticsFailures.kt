/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics

import androidx.annotation.NonNull

/**
 * Factory class which may be used for creating of [AnalyticsFailure] instances for a desired
 * destination.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
object AnalyticsFailures {

    /**
     * Creates a new instance of analytics failure with the given [cause] and single [destination]
     * where should be the new failure delivered.
     *
     * @param cause The cause due to which should be the new failure logged.
     * @param destination Name of the desired destination where should be the new failure delivered.
     * @return Analytics failure ready to be logged.
     */
    @JvmStatic fun create(@NonNull cause: Throwable, @NonNull destination: String): AnalyticsFailure = create(cause, hashSetOf(destination))

    /**
     * Creates a new instance of analytics failure with the given [cause] and [destinations] where
     * should be the new failure delivered.
     *
     * @param cause The cause due to which should be the new failure logged.
     * @param destinations Names of the desired destinations where should be the new failure delivered.
     * @return Analytics failure ready to be logged.
     */
    @JvmStatic fun create(@NonNull cause: Throwable, @NonNull destinations: Set<String>): AnalyticsFailure = FailureImpl(cause, destinations)

    /**
     * An [AnalyticsFailure] implementation.
     *
     * @author Martin Albedinsky
     * @since 1.0
     *
     * @constructor Creates a new instance of FailureImpl with the given [cause] and the specified
     * [destinations].
     * @param cause The cause due to which will be the new failure logged.
     * @param destinations Names of the desired destinations where should be the new failure delivered.
     */
    private class FailureImpl(private val cause: Throwable, private val destinations: Set<String>) : AnalyticsFailure {

        /*
         */
        override fun getDestinations() = destinations

        /*
         */
        override fun getCause() = cause
    }
}