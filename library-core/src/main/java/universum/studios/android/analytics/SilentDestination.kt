/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics

import androidx.annotation.NonNull
import androidx.annotation.VisibleForTesting

/**
 * An [AnalyticsDestination] implementation which silently ignores all event logging requests.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
internal object SilentDestination : AnalyticsDestination {

    /*
     * Companion ===================================================================================
     */

    /**
     * Name of the Silent analytics destination.
     */
    @VisibleForTesting internal const val NAME = ""

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /*
     * Initialization ==============================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /*
     */
    @NonNull override fun getName() = NAME

    /*
     */
    override fun logEvent(@NonNull event: AnalyticsEvent) {}

    /*
     */
    override fun logFailure(@NonNull failure: AnalyticsFailure) {}

    /*
     * Inner classes ===============================================================================
     */
}