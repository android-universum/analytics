/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics

import androidx.annotation.NonNull

/**
 * Interface declaring basic layer for logging of [AnalyticsEvents][AnalyticsEvent] and
 * [AnalyticsFailures][AnalyticsFailure].
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
interface Analytics {

    /*
     * Companion ===================================================================================
     */

    /**
     * Contract for [Analytics] element.
     */
    companion object Contract {

        /**
         * Empty implementation (NULL object) which doesn't perform any *logging* logic.
         */
        @NonNull internal val EMPTY: Analytics = object : Analytics {

            /*
             */
            override fun logEvent(event: AnalyticsEvent) {}

            /*
             */
            override fun logFailure(failure: AnalyticsFailure) {}
        }

        /**
         * Returns an empty instance of [Analytics] which may be used in cases where an instance of
         * concrete Analytics implementation is not needed.
         *
         * @return Empty analytics ready to be used.
         */
        @NonNull fun empty(): Analytics = EMPTY
    }

    /*
     * Methods =====================================================================================
     */

    /**
     * Logs the given analytics [event] to all analytics destinations determined by destination
     * names specified for the event.
     *
     * @param event The desired event to be logged.
     *
     * @see AnalyticsEvent.getDestinations
     * @see logFailure
     */
    fun logEvent(@NonNull event: AnalyticsEvent)

    /**
     * Logs the given [failure] into this destination.
     *
     * @param failure The desired failure to be logged.
     *
     * @see logEvent
     */
    fun logFailure(@NonNull failure: AnalyticsFailure)
}