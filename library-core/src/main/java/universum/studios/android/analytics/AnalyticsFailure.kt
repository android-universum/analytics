/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics

import androidx.annotation.NonNull

/**
 * Declaration for events that may be used to **analyze failures** occurring in an application. Each
 * analytics failure is required to provide [cause][getCause] due to which is such failure reported.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see AnalyticsEvent
 */
interface AnalyticsFailure : Event {

    /*
     * Companion ===================================================================================
     */

    /*
     * Interface ===================================================================================
     */

    /**
     * Basic interface for builders which may be used to build instances of [AnalyticsFailure] implementations.
     *
     * @param T Type of the analytics failure of which instance this builder can build.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    interface Builder<out T : AnalyticsFailure> : universum.studios.android.analytics.Event.Builder<T>

    /*
     * Methods =====================================================================================
     */

    /**
     * Returns the cause of this failure.
     *
     * @return Cause due to which has been this analytics failure created.
     */
    @NonNull fun getCause(): Throwable
}