/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics

import androidx.annotation.NonNull
import java.io.Serializable

/**
 * Declaration for events that may be used to **analyze user behaviour** in an application. Each
 * analytics event is required to have its unique [type][getType] among all analytics events and to
 * provide set of [attributes][getAttributes] which may be used to closely analyze such event.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see AnalyticsFailure
 */
interface AnalyticsEvent : Event {

    /*
     * Companion ===================================================================================
     */

    /*
     * Interface ===================================================================================
     */

    /**
     * Basic interface for builders which may be used to build instances of [AnalyticsEvent] implementations.
     *
     * @param T Type of the analytics event of which instance this builder can build.
     *
     * @author Martin Albedinsky
     */
    interface Builder<out T : AnalyticsEvent> : Event.Builder<T>

    /*
     * Methods =====================================================================================
     */

    /**
     * Returns the type which may be used to uniquely identify this analytics event.
     *
     * @return Type of this analytics event.
     */
    @NonNull fun getType(): String

    /**
     * Returns the attributes supplied for this analytics event.
     *
     * @return This event's attributes. If no attributes has been specified, this will be an empty map.
     */
    @NonNull fun getAttributes(): Map<String, Serializable>
}