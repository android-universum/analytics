/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics

import android.util.Log
import androidx.annotation.NonNull
import androidx.annotation.VisibleForTesting

/**
 * An [Analytics] facade layer for multiple [AnalyticsDestinations][AnalyticsDestination].
 * Desired destinations may be added/registered via [AnalyticsFacade.FacadeBuilder.addDestination]
 * where each destination is registered by its **unique name**. Destination name is then used by the
 * facade instance in order to find the desired destination for each event passed either to
 * [logEvent] or to [logFailure].
 *
 * If there is no destination registered a *silent destination* is used which does not perform any
 * logging.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of AnalyticsFacade with the given `builder's` configuration.
 * @param builder The builder with configuration for the new facade.
 */
abstract class AnalyticsFacade protected constructor(@NonNull builder: FacadeBuilder<*>) : Analytics {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object {

        /**
         * Log TAG.
         */
        internal const val TAG = "AnalyticsFacade"

        /**
         * Destination that is used as fallback destination when a desired destination for a specific
         * event is not found.
         */
        @VisibleForTesting internal val SILENT_DESTINATION = SilentDestination
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * Registry containing all added destinations mapped to their names. This facade picks among these
     * destinations the desired one for each event passed either to [logEvent] or to [logFailure] by
     * the destination name specified for such events.
     *
     * @see [findDestination]
     */
    private val destinationsRegistry = builder.destinations

    /*
     * Initialization ==============================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /**
     * Delivers the given analytics [event] to all analytics destinations determined by destination
     * names specified for the event.
     *
     * @param event The desired event to be delivered and logged.
     *
     * @see AnalyticsEvent.getDestinations
     * @see logFailure
     */
    override fun logEvent(@NonNull event: AnalyticsEvent) = event.getDestinations().forEach { findDestination(it).logEvent(event) }

    /**
     * Delivers the given analytics [failure] to all analytics destinations determined by destination
     * names specified for the failure.
     *
     * @param failure The desired failure to be delivered and logged.
     *
     * @see AnalyticsFailure.getDestinations
     * @see logEvent
     */
    override fun logFailure(@NonNull failure: AnalyticsFailure) = failure.getDestinations().forEach { findDestination(it).logFailure(failure) }

    /**
     * Finds destination with the given [name] among destinations registered for this analytics facade.
     *
     * @param name Name of the desired destination to find.
     * @return Registered destination with the specified name or [SILENT_DESTINATION] if no such destination
     * has been registered for this facade.
     */
    @VisibleForTesting internal fun findDestination(name: String): AnalyticsDestination {
        if (destinationsRegistry.containsKey(name)) {
            return destinationsRegistry[name]!!
        }
        Log.d(TAG, "No destination with name('$name') found. Falling back to Silent destination.")
        return SILENT_DESTINATION
    }

    /*
     * Inner classes ===============================================================================
     */

    /**
     * Base builder which should be used by by [AnalyticsFacade] implementation in order to implement
     * its specific builder.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    abstract class FacadeBuilder<B : FacadeBuilder<B>> {

        /**
         * See [AnalyticsFacade.destinationsRegistry].
         */
        internal val destinations: HashMap<String, AnalyticsDestination> = hashMapOf()

        /**
         * Obtains reference to this builder instance which is used for purpose of methods chaining.
         *
         * @return This builder instance.
         */
        protected abstract val self: B

        /**
         * Adds/registers the specified [destination] among destinations which should be used/recognized
         * by the new analytics facade.
         *
         * The created facade will then pick among these destinations the desired ones for each event
         * passed either to [logEvent] or to [logFailure] by destination names specified for such
         * events.
         *
         * @return This builder to allow methods chaining.
         */
        fun addDestination(@NonNull destination: AnalyticsDestination): B {
            this.destinations[destination.getName()] = destination
            return self
        }

        /**
         * Builds a new instance of analytics facade with the desired destinations.
         *
         * @return New analytics facade implementation ready to be used.
         */
        @NonNull abstract fun build(): AnalyticsFacade
    }
}