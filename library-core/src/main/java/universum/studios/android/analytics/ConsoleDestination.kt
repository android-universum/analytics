/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics

import androidx.annotation.NonNull
import androidx.annotation.VisibleForTesting

import universum.studios.android.logging.Logger

/**
 * An [AnalyticsDestination] implementation which logs all received [events][AnalyticsEvent] and
 * [failures][AnalyticsFailure] into console using [Logger].
 *
 * An instance of this destination may be created via [ConsoleDestination.create] factory method which
 * takes as argument the desired logger.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of ConsoleDestination with the given `logger`.
 * @param logger The desired logger that will be used to log all received events and failures into
 * console output.
 */
class ConsoleDestination private constructor(@NonNull private val logger: Logger) : AnalyticsDestination {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object {

        /**
         * Name of the Console analytics destination.
         */
        const val NAME = "CONSOLE"

        /**
         * Logging tag for [logger].
         */
        @VisibleForTesting internal const val TAG = "ConsoleDestination"

        /**
         * Creates a new instance of ConsoleDestination with the given [logger].
         *
         * @param logger The desired logger that will be used to log all received events and failures
         * into console output.
         * @return Analytics destination ready to be used.
         */
        @JvmStatic @NonNull fun create(@NonNull logger: Logger): AnalyticsDestination = ConsoleDestination(logger)
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /*
     * Initialization ==============================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /*
     */
    @NonNull override fun getName() = NAME

    /*
     */
    override fun logEvent(@NonNull event: AnalyticsEvent) = logger.i(TAG, "Received event => $event")

    /*
     */
    override fun logFailure(@NonNull failure: AnalyticsFailure) = logger.w(TAG, "Received failure => $failure")

    /*
     * Inner classes ===============================================================================
     */
}