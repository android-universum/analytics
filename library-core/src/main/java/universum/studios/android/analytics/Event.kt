/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics

import androidx.annotation.NonNull

/**
 * Specifies a basic interface for an event which may be delivered to a desired destination.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
interface Event {

    /*
     * Companion ===================================================================================
     */

    /*
     * Interface ===================================================================================
     */

    /**
     * Basic interface for builders which may be used to build instances of [Event] implementations.
     *
     * @param T Type of the event of which instance this builder can build.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    interface Builder<out T : Event> {

        /**
         * Builds a new instance of event specific for this builder.
         *
         * @return New instance of the desired event ready to be used.
         * @throws AssertionError If some of the required arguments is not specified.
         */
        @NonNull fun build(): T
    }

    /*
     * Methods =====================================================================================
     */

    /**
     * Returns the set of destinations where this event should be delivered.
     *
     * @return Destinations of this event.
     */
    @NonNull fun getDestinations(): Set<String>
}