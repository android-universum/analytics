Analytics-Core
===============

This module contains core elements.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aanalytics/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aanalytics/_latestVersion)

### Gradle ###

    compile "universum.studios.android:analytics-core:${DESIRED_VERSION}@aar"

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [AnalyticsFacade](https://bitbucket.org/android-universum/analytics/src/main/library-core/src/main/java/universum/studios/android/analytics/AnalyticsFacade.kt)
- [AnalyticsEvent](https://bitbucket.org/android-universum/analytics/src/main/library-core/src/main/java/universum/studios/android/analytics/AnalyticsEvent.kt)
- [AnalyticsDestination](https://bitbucket.org/android-universum/analytics/src/main/library-core/src/main/java/universum/studios/android/analytics/AnalyticsDestination.kt)
