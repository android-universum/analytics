Analytics-Screen
===============

This module contains elements which may be used to track **screen** related events.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aanalytics/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aanalytics/_latestVersion)

### Gradle ###

    compile "universum.studios.android:analytics-screen:${DESIRED_VERSION}@aar"

_depends on:_
[analytics-core](https://bitbucket.org/android-universum/analytics/src/main/library-core),
[analytics-event](https://bitbucket.org/android-universum/analytics/src/main/library-event)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [ScreenAnalytics](https://bitbucket.org/android-universum/analytics/src/main/library-screen/src/main/java/universum/studios/android/analytics/screen/ScreenAnalytics.kt)
- [AnalyticsScreen](https://bitbucket.org/android-universum/analytics/src/main/library-screen/src/main/java/universum/studios/android/analytics/screen/AnalyticsScreen.kt)
- [ScreenViewEvent](https://bitbucket.org/android-universum/analytics/src/main/library-screen/src/main/java/universum/studios/android/analytics/screen/ScreenViewEvent.kt)
