/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics.screen

import android.app.Activity
import android.app.Application
import android.app.Application.ActivityLifecycleCallbacks
import android.os.Bundle
import androidx.annotation.NonNull
import universum.studios.android.analytics.AnalyticsFacade

/**
 * Analytics for reporting of screen related events. May created via [ScreenAnalytics.Builder] and
 * then installed for a desired [Application] via [install].
 *
 * **Reported events:**
 * - [ScreenViewEvent]
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
class ScreenAnalytics internal constructor(builder: Builder) {

    /*
     * Companion ===================================================================================
     */

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * Analytics facade that should be used for screen events logging.
     */
    private val analytics = builder.analytics

    /**
     * Names of all analytics destinations where should be screen events delivered.
     */
    private val destinations = builder.destinations

    /**
     * Listener which is registered for listening of changes in activity's lifecycle state that are
     * used to determine which screen related events to log.
     *
     * When initialized, this analytics instance is installed for a specific application, when `null`
     * this analytics instance is not installed for any application.
     *
     * @see install
     * @see uninstall
     */
    private var activityLifecycleListener: ActivityLifecycleListener? = null

    /*
     * Initialization ==============================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /**
     * Installs this screen analytics for the specified [application].
     *
     * @param application The desired application for which to install screen analytics.
     * @throws IllegalStateException If this analytics is already installed.
     *
     * @see uninstall
     */
    fun install(@NonNull application: Application) {
        if (activityLifecycleListener != null) {
            throw IllegalStateException("Already installed!")
        }
        this.activityLifecycleListener = ActivityLifecycleListener(analytics, destinations)
        application.registerActivityLifecycleCallbacks(activityLifecycleListener)
    }

    /**
     * Uninstalls this screen analytics for the specified [application].
     *
     * Does nothing if this analytics instance is not installed.
     *
     * @param application The desired application for which is this screen analytics installed and
     * should be now uninstalled.
     *
     * @see install
     */
    fun uninstall(@NonNull application: Application) {
        if (activityLifecycleListener != null) {
            application.unregisterActivityLifecycleCallbacks(activityLifecycleListener)
            this.activityLifecycleListener = null
        }
    }

    /*
     * Inner classes ===============================================================================
     */

    /**
     * Builder which may be used to build instances of [ScreenAnalytics].
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    class Builder {

        /**
         * See [ScreenAnalytics.analytics].
         */
        internal lateinit var analytics: AnalyticsFacade

        /**
         * See [ScreenAnalytics.destinations].
         */
        internal val destinations: HashSet<String> = HashSet(1)

        /**
         * Specifies an analytics facade that should be used for screen events logging.
         *
         * @param analytics  The desired analytics facade instance to be used for events logging.
         * @return This builder to allow methods chaining.
         */
        fun analytics(@NonNull analytics: AnalyticsFacade): Builder {
            this.analytics = analytics
            return this
        }

        /**
         * Specifies multiple destinations where should be screen events delivered.
         *
         * @param destinations Names of the desired destinations where should be events delivered.
         * @return This builder to allow methods chaining.
         */
        fun destinations(@NonNull vararg destinations: String): Builder {
            this.destinations.clear()
            this.destinations.addAll(destinations)
            return this
        }

        /**
         * Specifies multiple destinations where should be screen events delivered.
         *
         * @param destinations Set of names of the desired destinations where should be events delivered.
         * @return This builder to allow methods chaining.
         */
        fun destinations(@NonNull destinations: Set<String>): Builder {
            this.destinations.clear()
            this.destinations.addAll(destinations)
            return this
        }

        /**
         * Builds a new instance of ScreenAnalytics.
         *
         * @return Screen analytics ready to be used.
         */
        @NonNull fun build() = ScreenAnalytics(this)
    }

    /**
     * A listener which listens for activity lifecycle events, particularly *onStarted(...)* event,
     * and logs [ScreenViewEvent] for the appropriate activity along with its **analytic name** if
     * such activity is instance of [AnalyticsScreen].
     *
     * @see AnalyticsScreen.getAnalyticName
     */
    private class ActivityLifecycleListener(
            /**
             * The desired analytics facade instance that should be used for events logging.
             */
            private val analytics: AnalyticsFacade,
            /**
             * Names of the desired analytics destinations where should be events delivered.
             */
            private val destinations: Set<String>) : ActivityLifecycleCallbacks {

        /*
         */
        override fun onActivityPaused(activity: Activity) {}

        /*
         */
        override fun onActivityResumed(activity: Activity) {}

        /*
         */
        override fun onActivityStarted(activity: Activity) {
            if (activity is AnalyticsScreen) {
                this.analytics.logEvent(ScreenViewEvent.builder().destinations(destinations).screenName(activity.getAnalyticName()).build())
            }
        }

        /*
         */
        override fun onActivityDestroyed(activity: Activity) {}

        /*
         */
        override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle?) {}

        /*
         */
        override fun onActivityStopped(activity: Activity) {}

        /*
         */
        override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {}
    }
}