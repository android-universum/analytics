/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics.screen

import androidx.annotation.NonNull

/**
 * A marking interface which may be used to mark a desired `Activity` or `Fragment` in order to
 * inform [ScreenAnalytics] that [ScreenViewEvent] should be logged for such component.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
interface AnalyticsScreen {

    /**
     * Returns the unique name of this screen for analytics purpose only.
     *
     * @return Analytic name of this screen.
     */
    @NonNull fun getAnalyticName(): String
}