/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics.screen

import androidx.annotation.NonNull
import universum.studios.android.analytics.event.BaseAnalyticsEvent

/**
 * Analytics event which may be used to identify that a specific **screen** has been **viewed** by a
 * user.
 *
 * Instances of this event may be created via [ScreenViewEvent.Builder] and desired attribute values
 * may be supplied via predefined builder's chainable methods.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see ScreenViewEvent.builder
 *
 * @constructor Creates a new instance of ScreenViewEvent with the given `builder's` configuration.
 * @param builder The builder with configuration for the new event.
 */
class ScreenViewEvent internal constructor(@NonNull builder: Builder) : BaseAnalyticsEvent(builder) {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object {

        /**
         * Type uniquely identifying [ScreenViewEvent].
         */
        internal const val TYPE = "screen_view"

        /**
         * Returns a new instance of screen view event [Builder].
         *
         * @return Builder ready to be used to build instance of a desired event.
         */
        @JvmStatic @NonNull fun builder() = Builder()
    }

    /**
     * Defines general attributes for which may be supplied theirs corresponding values via [ScreenViewEvent].
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    object Attribute {

        /**
         * Attribute for supplying name of the screen that has been viewed.
         */
        const val NAME = "screen_name"
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /*
     * Initialization ==============================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /*
     * Inner classes ===============================================================================
     */

    /**
     * A builder which may be used to build instances of [ScreenViewEvent].
     *
     * @author Martin Albedinsky
     * @since 1.0
     *
     * @constructor Creates a new instance of Builder with [TYPE] used as `eventType`.
     */
    class Builder internal constructor() : BaseBuilder<Builder, ScreenViewEvent>(TYPE) {

        /*
         */
        override val self = this

        /**
         * Specifies a name of the screen that has been viewed.
         *
         * This value is **required** and it is associated with [NAME][Attribute.NAME] attribute key.
         *
         * @return This builder to allow methods chaining.
         */
        fun screenName(@NonNull name: String) = putAttribute(Attribute.NAME, name)

        /*
         */
        @NonNull override fun build(): ScreenViewEvent {
            assertHasAttributeValues(Attribute.NAME)
            return ScreenViewEvent(this)
        }
    }
}