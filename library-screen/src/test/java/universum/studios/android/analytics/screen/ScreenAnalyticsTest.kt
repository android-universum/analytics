/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics.screen

import android.app.Activity
import android.app.Application
import android.app.Application.ActivityLifecycleCallbacks
import android.os.Bundle
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`
import org.hamcrest.core.IsNull.notNullValue
import org.junit.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoInteractions
import org.mockito.Mockito.verifyNoMoreInteractions
import universum.studios.android.analytics.AnalyticsDestination
import universum.studios.android.analytics.AnalyticsFacade
import universum.studios.android.analytics.screen.ScreenAnalyticsTest.TestAnalytics.Builder
import universum.studios.android.test.TestCase
import universum.studios.android.test.kotlin.KotlinArgumentMatchers

/**
 * @author Martin Albedinsky
 */
class ScreenAnalyticsTest : TestCase() {

    @Test fun testBuilderAnalytics() {
        // Arrange:
        val mockAnalytics = mock(AnalyticsFacade::class.java)
        // Act:
        val builder = ScreenAnalytics.Builder().analytics(mockAnalytics)
        // Assert:
        assertThat(builder.analytics, `is`(mockAnalytics))
    }

    @Test fun testBuilderDestinationsAsVarArgs() {
        // Act:
        val builder = ScreenAnalytics.Builder().destinations("TEST_SCREEN#1", "TEST_SCREEN#2")
        // Assert:
        assertThat(builder.destinations, `is`(setOf("TEST_SCREEN#1", "TEST_SCREEN#2")))
    }

    @Test fun testBuilderDestinationsAsSet() {
        // Act:
        val builder = ScreenAnalytics.Builder().destinations(setOf("TEST_SCREEN"))
        // Assert:
        assertThat(builder.destinations, `is`(setOf("TEST_SCREEN")))
    }

    @Test fun testInstantiation() {
        // Arrange:
        val mockAnalytics = mock(AnalyticsFacade::class.java)
        // Act:
        val screenAnalytics =  ScreenAnalytics.Builder().analytics(mockAnalytics).destinations(setOf("TEST_SCREEN")).build()
        // Assert:
        assertThat(screenAnalytics, `is`(notNullValue()))
        verifyNoInteractions(mockAnalytics)
    }

    @Test fun testInstall() {
        // Arrange:
        val mockApplication = mock(Application::class.java)
        val mockAnalytics = mock(AnalyticsFacade::class.java)
        val screenAnalytics =  ScreenAnalytics.Builder().analytics(mockAnalytics).destinations(setOf("TEST_SCREEN")).build()
        // Act:
        screenAnalytics.install(mockApplication)
        // Assert:
        verify(mockApplication).registerActivityLifecycleCallbacks(any(ActivityLifecycleCallbacks::class.java))
    }

    @Test(expected = IllegalStateException::class)
    fun testInstallWhenAlreadyInstalled() {
        // Arrange:
        val mockApplication = mock(Application::class.java)
        val mockAnalytics = mock(AnalyticsFacade::class.java)
        val screenAnalytics =  ScreenAnalytics.Builder().analytics(mockAnalytics).destinations(setOf("TEST_SCREEN")).build()
        screenAnalytics.install(mockApplication)
        // Act:
        screenAnalytics.install(mockApplication)
        // Assert:
        verify(mockApplication).registerActivityLifecycleCallbacks(any(ActivityLifecycleCallbacks::class.java))
    }

    @Test fun testUninstall() {
        // Arrange:
        val mockApplication = mock(Application::class.java)
        val mockAnalytics = mock(AnalyticsFacade::class.java)
        val screenAnalytics =  ScreenAnalytics.Builder().analytics(mockAnalytics).destinations(setOf("TEST_SCREEN")).build()
        screenAnalytics.install(mockApplication)
        Mockito.clearInvocations(mockApplication)
        // Act:
        screenAnalytics.uninstall(mockApplication)
        // Assert:
        verify(mockApplication).unregisterActivityLifecycleCallbacks(any(ActivityLifecycleCallbacks::class.java))
        verifyNoMoreInteractions(mockApplication)
    }

    @Test fun testUninstallWhenNotInstalled() {
        // Arrange:
        val mockApplication = mock(Application::class.java)
        val mockAnalytics = mock(AnalyticsFacade::class.java)
        val screenAnalytics =  ScreenAnalytics.Builder().analytics(mockAnalytics).destinations(setOf("TEST_SCREEN")).build()
        // Act:
        screenAnalytics.uninstall(mockApplication)
        // Assert:
        verifyNoInteractions(mockApplication)
    }

    @Test fun testActivityOnCreate() {
        // Arrange:
        val application = TestApplication()
        // Act + Assert:
        this.testNotLoggedActivityLifecycleEvent(application, Runnable { application.notifyActivityCreated(TestAnalyticsActivity()) })
    }

    @Test fun testActivityOnStart() {
        // Arrange:
        val mockDestination = mock(AnalyticsDestination::class.java)
        `when`(mockDestination.getName()).thenReturn("TEST_SCREEN")
        val analytics = Builder().addDestination(mockDestination).build()
        verify(mockDestination).getName()
        val application = TestApplication()
        ScreenAnalytics.Builder().analytics(analytics).destinations(setOf("TEST_SCREEN")).build().install(application)
        // Act:
        application.notifyActivityStarted(TestAnalyticsActivity())
        // Assert:
        verify(mockDestination).logEvent(KotlinArgumentMatchers.any(ScreenViewEvent::class.java))
        verifyNoMoreInteractions(mockDestination)
    }

    @Test fun testActivityOnResume() {
        // Arrange:
        val application = TestApplication()
        // Act + Assert:
        this.testNotLoggedActivityLifecycleEvent(application, Runnable { application.notifyActivityResumed(TestAnalyticsActivity()) })
    }

    @Test fun testActivityOnPause() {
        // Arrange:
        val application = TestApplication()
        // Act + Assert:
        this.testNotLoggedActivityLifecycleEvent(application, Runnable { application.notifyActivityPaused(TestAnalyticsActivity()) })
    }

    @Test fun testActivityOnStop() {
        // Arrange:
        val application = TestApplication()
        // Act + Assert:
        this.testNotLoggedActivityLifecycleEvent(application, Runnable { application.notifyActivityStopped(TestAnalyticsActivity()) })
    }

    @Test fun testActivityOnSaveInstanceState() {
        // Arrange:
        val application = TestApplication()
        // Act + Assert:
        this.testNotLoggedActivityLifecycleEvent(application, Runnable { application.notifyActivitySaveState(TestAnalyticsActivity()) })
    }

    @Test fun testActivityOnDestroy() {
        // Arrange:
        val application = TestApplication()
        // Act + Assert:
        this.testNotLoggedActivityLifecycleEvent(application, Runnable { application.notifyActivityDestroyed(TestAnalyticsActivity()) })
    }

    private fun testNotLoggedActivityLifecycleEvent(application: TestApplication, eventTrigger: Runnable) {
        // Arrange:
        val mockDestination = mock(AnalyticsDestination::class.java)
        `when`(mockDestination.getName()).thenReturn("TEST_SCREEN")
        val analytics = Builder().addDestination(mockDestination).build()
        Mockito.clearInvocations(mockDestination)
        ScreenAnalytics.Builder().analytics(analytics).destinations(setOf("TEST_SCREEN")).build().install(application)
        // Act:
        eventTrigger.run()
        // Assert:
        verify(mockDestination, times(0)).logEvent(KotlinArgumentMatchers.any(ScreenViewEvent::class.java))
        verifyNoMoreInteractions(mockDestination)
    }

    @Test fun testStartActivityThatIsNotAnalyticsScreen() {
        // Arrange:
        val mockDestination = mock(AnalyticsDestination::class.java)
        `when`(mockDestination.getName()).thenReturn("TEST_SCREEN")
        val analytics = Builder().addDestination(mockDestination).build()
        verify(mockDestination).getName()
        val application = TestApplication()
        ScreenAnalytics.Builder().analytics(analytics).destinations(setOf("TEST_SCREEN")).build().install(application)
        // Act:
        application.notifyActivityStarted(TestActivity())
        // Assert:
        verify(mockDestination, times(0)).logEvent(KotlinArgumentMatchers.any(ScreenViewEvent::class.java))
        verifyNoMoreInteractions(mockDestination)
    }

    private class TestApplication : Application() {

        val activityLifecycleCallbacks = mutableListOf<ActivityLifecycleCallbacks>()

        override fun registerActivityLifecycleCallbacks(callback: ActivityLifecycleCallbacks) {
            activityLifecycleCallbacks.add(callback)
        }

        fun notifyActivityCreated(activity: Activity) = activityLifecycleCallbacks.forEach { it.onActivityCreated(activity, null) }

        fun notifyActivityStarted(activity: Activity) = activityLifecycleCallbacks.forEach { it.onActivityStarted(activity) }

        fun notifyActivityResumed(activity: Activity) = activityLifecycleCallbacks.forEach { it.onActivityResumed(activity) }

        fun notifyActivityPaused(activity: Activity) = activityLifecycleCallbacks.forEach { it.onActivityPaused(activity) }

        fun notifyActivityStopped(activity: Activity) = activityLifecycleCallbacks.forEach { it.onActivityStopped(activity) }

        fun notifyActivitySaveState(activity: Activity) = activityLifecycleCallbacks.forEach { it.onActivitySaveInstanceState(activity, Bundle.EMPTY) }

        fun notifyActivityDestroyed(activity: Activity) = activityLifecycleCallbacks.forEach { it.onActivityDestroyed(activity) }
    }

    @Suppress("unused")
    private class TestAnalytics(builder: Builder) : AnalyticsFacade(builder) {

        class Builder : FacadeBuilder<Builder>() {
            override val self = this
            override fun build() = TestAnalytics(this)
        }
    }

    private class TestAnalyticsActivity : Activity(), AnalyticsScreen {

        override fun getAnalyticName() = "TestAnalyticsActivity"
    }

    private class TestActivity : Activity()
}