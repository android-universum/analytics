/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics.screen

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`
import org.hamcrest.core.IsNot.not
import org.hamcrest.core.IsNull.notNullValue
import org.junit.Test
import universum.studios.android.test.TestCase
import java.io.Serializable

/**
 * @author Martin Albedinsky
 */
class ScreenViewEventTest : TestCase() {

    @Test fun testType() {
        // Act + Assert:
        assertThat(ScreenViewEvent.TYPE, `is`("screen_view"))
    }

    @Test fun testAttributes() {
        // Act + Assert:
        assertThat(ScreenViewEvent.Attribute.NAME, `is`("screen_name"))
    }

    @Test fun testBuilder() {
        // Act:
        val builder = ScreenViewEvent.builder()
        // Assert:
        assertThat(builder, `is`(notNullValue()))
        assertThat(builder, `is`(not(ScreenViewEvent.builder())))
    }

    @Test fun testInstantiation() {
        // Act:
        val event = ScreenViewEvent.builder().screenName("UNKNOWN").build()
        // Assert:
        assertThat(event, `is`(notNullValue()))
        assertThat(event.getType(), `is`(ScreenViewEvent.TYPE))
        assertThat(event.getDestinations().isEmpty(), `is`(true))
        val attributes = event.getAttributes()
        assertThat(attributes.size, `is`(1))
        assertThat(attributes[ScreenViewEvent.Attribute.NAME], `is`<Serializable>("UNKNOWN"))
    }

    @Test(expected = AssertionError::class)
    fun testInstantiationWithoutScreenName() {
        // Act:
        ScreenViewEvent.builder().build()
    }
}