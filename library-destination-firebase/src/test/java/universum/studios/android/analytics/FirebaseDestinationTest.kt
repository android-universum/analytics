/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics

import com.google.firebase.analytics.FirebaseAnalytics
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`
import org.hamcrest.core.IsNot.not
import org.hamcrest.core.IsNull.notNullValue
import org.junit.Ignore
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import universum.studios.android.test.AndroidTestCase
import java.io.Serializable
import java.lang.reflect.InvocationTargetException

/**
 * @author Martin Albedinsky
 */
class FirebaseDestinationTest : AndroidTestCase() {

    @Test fun testName() {
        // Act + Assert:
        assertThat(FirebaseDestination.NAME, `is`("FIREBASE"))
    }

    @Test(expected = IllegalAccessException::class)
    fun testMappersInstantiation() {
        // Act:
        FirebaseDestination.Mappers::class.java.newInstance()
    }

    @Test(expected = InvocationTargetException::class)
    fun testMappersInstantiationWithAccessibleConstructor() {
        // Act:
        FirebaseDestination.Mappers::class.java.getDeclaredConstructor().apply { isAccessible = true }.newInstance()
    }

    @Test fun testMappersAttributesToBundle() {
        // Act:
        val bundle = FirebaseDestination.Mappers.ATTRIBUTES_TO_BUNDLE.map(mapOf(
                Pair<String, Serializable>("ATTR.Number", 10),
                Pair<String, Serializable>("ATTR.Text", "Test")
        ))
        // Assert:
        assertThat(bundle, `is`(notNullValue()))
        assertThat(bundle.size(), `is`(2))
        assertThat(bundle.getString("ATTR.Number"), `is`("10"))
        assertThat(bundle.getString("ATTR.Text"), `is`("Test"))
    }

    // TEST: however this test may be successfully executed on local machine ...
    @Ignore("Fails on CI server with java.lang.VerifyError.")
    @Test fun testCreate() {
        // Arrange:
        val firebaseAnalytics = FirebaseAnalytics.getInstance(context())
        // Act:
        val destination = FirebaseDestination.create(firebaseAnalytics)
        // Assert:
        assertThat(destination, `is`(notNullValue()))
        assertThat(destination.getName(), `is`(FirebaseDestination.NAME))
        assertThat(destination, `is`(not(FirebaseDestination.create(firebaseAnalytics))))
    }

    // TEST: however this test may be successfully executed on local machine ...
    @Ignore("Fails on CI server with java.lang.VerifyError.")
    @Test fun testLogEvent() {
        // Arrange:
        val destination = FirebaseDestination.create(FirebaseAnalytics.getInstance(context()))
        // Act + Assert:
        for (i in 1..10) {
            val mockEvent = mock(AnalyticsEvent::class.java)
            `when`(mockEvent.getType()).thenReturn("TestEvent")
            `when`(mockEvent.getAttributes()).thenReturn(emptyMap())
            destination.logEvent(mockEvent)
            verify(mockEvent).getAttributes()
        }
    }

    // TEST: however this test may be successfully executed on local machine ...
    @Ignore("Fails on CI server with java.lang.VerifyError.")
    @Test fun testLogFailure() {
        // Arrange:
        val destination = FirebaseDestination.create(FirebaseAnalytics.getInstance(context()))
        // Only assert that using this method for this destination doest not cause any troubles,
        // because FirebaseAnalytics does not support failures logging.
        // Act:
        destination.logFailure(mock(AnalyticsFailure::class.java))
    }
}