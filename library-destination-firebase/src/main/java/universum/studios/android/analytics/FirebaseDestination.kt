/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics

import android.os.Bundle
import androidx.annotation.NonNull
import androidx.annotation.VisibleForTesting
import com.google.firebase.analytics.FirebaseAnalytics
import java.io.Serializable

/**
 * An [AnalyticsDestination] implementation which dispatches all analytics events passed to [logEvent]
 * to the [FirebaseAnalytics] instance the destination has been created with.
 *
 * **Failures are not dispatched because _FirebaseAnalytics_ does not support such feature**.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of FirebaseDestination with the given `analytics` instance.
 * @param analytics The desired FirebaseAnalytics instance which should be used by the new destination
 * for events logging.
 */
class FirebaseDestination private constructor(@NonNull private val analytics: FirebaseAnalytics) : AnalyticsDestination {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object {

        /**
         * Name of the Firebase analytics destination.
         */
        const val NAME = "FIREBASE"

        /**
         * Creates a new instance of FirebaseDestination with the given [analytics] instance.
         *
         * @param analytics The desired FirebaseAnalytics instance which should be used by the new
         * destination for events logging.
         * @return Analytics destination ready to be used.
         */
        @JvmStatic @NonNull fun create(@NonNull analytics: FirebaseAnalytics) = FirebaseDestination(analytics)
    }

    /**
     * Class containing model mapper implementations used by this destination for mapping of analytics
     * event models specific for this analytics component to models that are specific for **Firebase API**.
     */
    @Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
    @VisibleForTesting internal class Mappers private constructor() {

        /**
         */
        companion object {

            /**
             * Mapper which maps [Map] of attributes to [Bundle].
             */
            val ATTRIBUTES_TO_BUNDLE = object : AnalyticsModelMapper<Map<String, Serializable>, Bundle> {

                /*
                 */
                @NonNull override fun map(@NonNull attributes: Map<String, Serializable>): Bundle {
                    val bundle = Bundle(attributes.size)
                    attributes.forEach { bundle.putString(it.key, it.value.toString()) }
                    return bundle
                }
            }
        }

        /**
         */
        init {
            // Not allowed to be instantiated publicly.
            throw UnsupportedOperationException()
        }
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /*
     * Initialization ==============================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /*
     */
    @NonNull override fun getName() = NAME

    /*
     */
    override fun logEvent(@NonNull event: AnalyticsEvent) = analytics.logEvent(event.getType(), Mappers.ATTRIBUTES_TO_BUNDLE.map(event.getAttributes()))

    /*
     */
    override fun logFailure(@NonNull failure: AnalyticsFailure) {}

    /*
     * Inner classes ===============================================================================
     */
}