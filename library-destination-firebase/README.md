Analytics-Destination-Firebase
===============

This module contains implementation of analytics destination for the _[Firebase](https://firebase.google.com/)_ tool.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aanalytics/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aanalytics/_latestVersion)

### Gradle ###

    compile "universum.studios.android:analytics-destination-firebase:${DESIRED_VERSION}@aar"

_depends on:_
[analytics-core](https://bitbucket.org/android-universum/analytics/src/main/library-core),
[analytics-event](https://bitbucket.org/android-universum/analytics/src/main/library-event)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [FirebaseDestination](https://bitbucket.org/android-universum/analytics/src/main/library-destination-firebase/src/main/java/universum/studios/android/analytics/FirebaseDestination.kt)
