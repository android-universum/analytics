Analytics-Event
===============

This module contains common analytics **events**.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aanalytics/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aanalytics/_latestVersion)

### Gradle ###

    compile "universum.studios.android:analytics-event:${DESIRED_VERSION}@aar"

_depends on:_
[analytics-core](https://bitbucket.org/android-universum/analytics/src/main/library-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:


- [GeneralAnalyticsEvent](https://bitbucket.org/android-universum/analytics/src/main/library-event/src/main/java/universum/studios/android/analytics/event/GeneralAnalyticsEvent.kt)
- [SignInEvent](https://bitbucket.org/android-universum/analytics/src/main/library-event/src/main/java/universum/studios/android/analytics/event/SignInEvent.kt)
