/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics.event

import androidx.annotation.NonNull

/**
 * Analytics event which may be used to identify a **sign in** procedure performed by a user.
 *
 * Instances of this event may be created via [SignInEvent.Builder] and desired attribute values
 * may be supplied via predefined builder's chainable methods.
 *
 * **Event attributes:**
 * - [METHOD][SignInEvent.Attribute.METHOD]
 * - [SUCCESS][SignInEvent.Attribute.SUCCESS]
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of SignInEvent with the given `builder's` configuration.
 * @param builder The builder with configuration for the new event.
 *
 * @see SignInEvent.builder
 */
class SignInEvent internal constructor(@NonNull builder: Builder) : BaseAnalyticsEvent(builder) {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object {

        /**
         * Type uniquely identifying [SignInEvent].
         */
        internal const val TYPE = "sign_in"

        /**
         * Returns a new instance of sign in event [Builder].
         *
         * The returned builder will have by default specified `false` value for [Attribute.SUCCESS]
         * attribute.
         *
         * @return Builder ready to be used to build instance of a desired event.
         */
        @JvmStatic @NonNull fun builder() = Builder()
    }

    /**
     * Defines general attributes for which may be supplied theirs corresponding values via [SignInEvent].
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    object Attribute {

        /**
         * Attribute for supplying name of the method used for sign in procedure.
         */
        const val METHOD = "method"

        /**
         * Attribute for supplying boolean flag indicating whether sign in procedure has been
         * successful or not.
         */
        const val SUCCESS = "success"
    }

    /**
     * Defines general methods used for sign in procedure.
     *
     * @author Martin Albedinsky
     */
    object Method {

        /**
         * Sign in method in which standard user credentials (username + password) are used.
         */
        const val PASSWORD = "PASSWORD"
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /*
     * Initialization ==============================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /*
     * Inner classes ===============================================================================
     */

    /**
     * A builder which may be used to build instances of [SignInEvent].
     *
     * @author Martin Albedinsky
     *
     * @constructor Creates a new instance of Builder with [TYPE] used as `eventType`.
     */
    class Builder internal constructor() : BaseBuilder<Builder, SignInEvent>(TYPE) {

        /*
         */
        override val self = this

        /**
         */
        init { putAttribute(Attribute.SUCCESS, false) }

        /**
         * Specifies a method used for a sign in procedure.
         *
         * This value is **required** and it is associated with [METHOD][Attribute.METHOD] attribute key.
         *
         * @return This builder to allow methods chaining.
         */
        fun method(@NonNull method: String) = putAttribute(Attribute.METHOD, method)

        /**
         * Specifies whether a sign in procedure has been successful or not.
         *
         * This value is **required** and it is associated with [SUCCESS][Attribute.SUCCESS] attribute key.
         *
         * @return This builder to allow methods chaining.
         */
        fun success(@NonNull success: Boolean) = putAttribute(Attribute.SUCCESS, success)

        /*
         */
        @NonNull override fun build(): SignInEvent {
            assertHasAttributeValues(Attribute.METHOD)
            return SignInEvent(this)
        }
    }
}