/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics.event

import androidx.annotation.NonNull

/**
 * Analytics event which may be used to identify a **sign out** procedure performed by a user.
 *
 * Instances of this event may be created via [SignOutEvent.Builder] and desired attribute values
 * may be supplied via predefined builder's chainable methods.
 *
 * **Event attributes:**
 * - [SUCCESS][SignOutEvent.Attribute.SUCCESS]
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of SignOutEvent with the given `builder's` configuration.
 * @param builder The builder with configuration for the new event.
 *
 * @see SignOutEvent.builder
 */
class SignOutEvent internal constructor(@NonNull builder: Builder) : BaseAnalyticsEvent(builder) {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object {

        /**
         * Type uniquely identifying [SignOutEvent].
         */
        internal const val TYPE = "sign_out"

        /**
         * Returns a new instance of sign out event [Builder].
         *
         * The returned builder will have by default specified `false` value for [Attribute.SUCCESS]
         * attribute.
         *
         * @return Builder ready to be used to build instance of a desired event.
         */
        @JvmStatic @NonNull fun builder() = Builder()
    }

    /**
     * Defines general attributes for which may be supplied theirs corresponding values via [SignOutEvent].
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    object Attribute {

        /**
         * Attribute for supplying boolean flag indicating whether sign in procedure has been
         * successful or not.
         */
        const val SUCCESS = "success"
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /*
     * Initialization ==============================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /*
     * Inner classes ===============================================================================
     */

    /**
     * A builder which may be used to build instances of [SignOutEvent].
     *
     * @author Martin Albedinsky
     * @since 1.0
     *
     * @constructor Creates a new instance of Builder with [TYPE] used as `eventType`.
     */
    class Builder internal constructor() : BaseBuilder<Builder, SignOutEvent>(TYPE) {

        /*
         */
        override val self = this

        /**
         */
        init { putAttribute(Attribute.SUCCESS, false) }

        /**
         * Specifies whether a sign out procedure has been successful or not.
         *
         * This value is **required** and it is associated with [SUCCESS][Attribute.SUCCESS] attribute key.
         *
         * @return This builder to allow methods chaining.
         */
        fun success(@NonNull success: Boolean) = putAttribute(Attribute.SUCCESS, success)

        /*
         */
        @NonNull override fun build(): SignOutEvent {
            return SignOutEvent(this)
        }
    }
}