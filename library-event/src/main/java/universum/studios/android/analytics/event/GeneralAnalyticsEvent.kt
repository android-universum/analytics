/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics.event

import androidx.annotation.NonNull
import universum.studios.android.analytics.event.GeneralAnalyticsEvent.Builder
import java.io.Serializable

/**
 * A general analytics event implementation which may be used to log analytics event without predefined
 * attributes.
 *
 * Instances of this event may be created via [GeneralAnalyticsEvent.Builder] and desired attribute
 * values may be supplied via [Builder.putAttribute] method.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of GeneralAnalyticsEvent with the given `builder's` configuration.
 * @param builder The builder with configuration for the new event.
 *
 * @see GeneralAnalyticsEvent.builder
 */
class GeneralAnalyticsEvent internal constructor(@NonNull builder: Builder) : BaseAnalyticsEvent(builder) {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object {

        /**
         * Returns a new instance of general event [Builder].
         *
         * @param eventType Unique type by which may be the new event identified.
         * @return Builder ready to be used to build instance of a desired event.
         */
        @JvmStatic fun builder(@NonNull eventType: String) = Builder(eventType)
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /*
     * Initialization ==============================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /*
     * Inner classes ===============================================================================
     */

    /**
     * A builder which may be used to build instances of [GeneralAnalyticsEvent].
     *
     * @author Martin Albedinsky
     * @since 1.0
     *
     * @constructor Creates a new instance of Builder with the given `eventType`.
     * @param eventType Unique type by which may be the new event identified.
     */
    class Builder internal constructor(@NonNull eventType: String) : BaseBuilder<Builder, GeneralAnalyticsEvent>(eventType) {

        /*
         */
        override val self = this

        /*
         */
        public override fun putAttribute(@NonNull key: String, @NonNull value: Serializable) = super.putAttribute(key, value)

        /*
         */
        @NonNull override fun build() = GeneralAnalyticsEvent(this)
    }
}