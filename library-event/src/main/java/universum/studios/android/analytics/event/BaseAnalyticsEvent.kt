/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics.event

import androidx.annotation.NonNull
import universum.studios.android.analytics.AnalyticsEvent
import universum.studios.android.analytics.AnalyticsEvent.Builder
import java.io.Serializable

/**
 * Base implementation of [AnalyticsEvent] which may be used as base for all specific analytics events.
 *
 * It is **preferred** that all analytics event implementations inherit from this base class.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of BaseAnalyticsEvent with the given `builder's` configuration.
 * @param builder The builder with configuration for the new event.
 */
abstract class BaseAnalyticsEvent protected constructor(@NonNull builder: BaseBuilder<*, *>) : AnalyticsEvent {

    /*
     * Companion ===================================================================================
     */

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * Unique type by which may be this analytics event identified.
     */
    private val type = builder.eventType

    /**
     * Set of destinations where should be this analytics event delivered.
     */
    private val destinations: Set<String> = builder.destinations

    /**
     * Map containing [Serializable] values mapped to unique attribute keys.
     */
    private val attributes: Map<String, Serializable> = builder.attributes

    /*
     * Initialization ==============================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /*
     */
    @NonNull final override fun getDestinations() = destinations

    /*
     */
    @NonNull final override fun getType() = type

    /*
     */
    @NonNull override fun getAttributes() = attributes

    /*
     */
    override fun toString(): String = "$type{destinations: $destinations, attributes: $attributes}"

    /*
     * Inner classes ===============================================================================
     */

    /**
     * Base implementation of [AnalyticsEvent.Builder] which should be used by [BaseAnalyticsEvent]
     * inheritance hierarchies.
     *
     * @param B Type of the builder used as return type for builder's chain-able methods.
     *
     * @author Martin Albedinsky
     * @since 1.0
     *
     * @constructor Creates a new instance of BaseBuilder with the given `eventType`.
     * @param eventType Unique type by which may be the new event identified.
     */
    abstract class BaseBuilder<B : BaseBuilder<B, T>, out T : BaseAnalyticsEvent> protected constructor(
            /**
             * See [BaseAnalyticsEvent.getType].
             */
            @NonNull internal val eventType: String) : Builder<T> {

        /**
         * Obtains reference to this builder instance which is used for purpose of methods chaining.
         *
         * @return This builder instance.
         */
        protected abstract val self: B

        /**
         * See [BaseAnalyticsEvent.destinations].
         */
        internal val destinations: HashSet<String> = HashSet(1)

        /**
         * See [BaseAnalyticsEvent.attributes].
         */
        internal val attributes: HashMap<String, Serializable> = HashMap(2)

        /**
         */
        init {
            if (eventType.isEmpty()) throw IllegalArgumentException("Event type cannot be empty!")
        }

        /**
         * Specifies single destination where should be the new event delivered.
         *
         * @param destination Name of the desired destination where should be event delivered.
         * @return This builder to allow methods chaining.
         *
         * @see destinations
         */
        fun destination(@NonNull destination: String): B {
            this.destinations.clear()
            this.destinations.add(destination)
            return self
        }

        /**
         * Specifies multiple destinations where should be the new event delivered.
         *
         * @param destinations Names of the desired destinations where should be event delivered.
         * @return This builder to allow methods chaining.
         *
         * @see destination
         */
        fun destinations(@NonNull vararg destinations: String): B {
            this.destinations.clear()
            this.destinations.addAll(destinations)
            return self
        }

        /**
         * Specifies multiple destinations where should be the new event delivered.
         *
         * @param destinations Set of names of the desired destinations where should be event delivered.
         * @return This builder to allow methods chaining.
         *
         * @see destination
         */
        fun destinations(@NonNull destinations: Set<String>): B {
            this.destinations.clear()
            this.destinations.addAll(destinations)
            return self
        }

        /**
         * Puts the given [value] for the specified attribute [key] into attributes map of this builder.
         *
         * If there is already value for the specified attribute key, it will be replaced by the new one.
         *
         * @param key Key of the desired attribute.
         * @param value The desired attribute value.
         * @return This builder to allow methods chaining.
         */
        protected open fun putAttribute(@NonNull key: String, @NonNull value: Serializable): B {
            this.attributes[key] = value
            return self
        }

        /**
         * Asserts that this builder has values for attributes with the specified [keys].
         *
         * @param keys Desired attribute keys for which to verify that values for them are specified
         * for this builder.
         * @throws IllegalArgumentException If at least value for one attribute is missing.
         */
        protected open fun assertHasAttributeValues(@NonNull vararg keys: String) {
            var missingAttributes = ""
            keys.forEach { if (attributes[it] == null) missingAttributes += "$it, " }
            if (missingAttributes.isNotEmpty()) {
                throw AssertionError("Missing values for attributes: [${missingAttributes.substring(0, missingAttributes.length - 2)}]!")
            }
        }
    }
}