/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics.event

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`
import org.hamcrest.core.IsNull.notNullValue
import org.junit.Test
import universum.studios.android.test.TestCase
import java.io.Serializable

/**
 * @author Martin Albedinsky
 */
class BaseAnalyticsEventTest : TestCase() {

    @Test fun testInstantiation() {
        // Act:
        val event = TestEvent.TestBuilder("TestEvent").build()
        // Assert:
        assertThat(event.getType(), `is`("TestEvent"))
        assertThat(event.getDestinations(), `is`(notNullValue()))
        assertThat(event.getDestinations().isEmpty(), `is`(true))
        assertThat(event.getAttributes(), `is`(notNullValue()))
        assertThat(event.getAttributes().isEmpty(), `is`(true))
    }

    @Test fun testBuilderDestination() {
        // Act:
        val event = TestEvent.TestBuilder("TestEvent")
                .destination("TestDestination")
                .build()
        val destinations = event.getDestinations()
        // Assert:
        assertThat(destinations.size, `is`(1))
        assertThat(destinations, `is`<Set<String>>(hashSetOf("TestDestination")))
    }

    @Test fun testBuilderDestinationsAsVarArgs() {
        // Act:
        val event = TestEvent.TestBuilder("TestEvent")
                .destinations("TestDestination#1", "TestDestination#2")
                .build()
        val destinations = event.getDestinations()
        // Assert:
        assertThat(destinations.size, `is`(2))
        assertThat(destinations, `is`<Set<String>>(hashSetOf("TestDestination#1", "TestDestination#2")))
    }

    @Test fun testBuilderDestinationsAsSet() {
        // Act:
        val event = TestEvent.TestBuilder("TestEvent")
                .destinations(setOf("TestDestination#1", "TestDestination#2"))
                .build()
        val destinations = event.getDestinations()
        // Assert:
        assertThat(destinations.size, `is`(2))
        assertThat(destinations, `is`<Set<String>>(hashSetOf("TestDestination#1", "TestDestination#2")))
    }

    @Test fun testBuilderPutAttribute() {
        // Act:
        val event = TestEvent.TestBuilder("TestEvent")
                .putAttribute("ATTR.Text", "TestUsers")
                .putAttribute("ATTR.Count", 10)
                .build()
        val attributes = event.getAttributes()
        // Assert:
        assertThat(attributes.size, `is`(2))
        assertThat(attributes["ATTR.Text"], `is`<Serializable>("TestUsers"))
        assertThat(attributes["ATTR.Count"], `is`<Serializable>(10))
    }

    @Test fun testBuilderAssertHasAttributeValuesWithValues() {
        // Act:
        TestEvent.TestBuilder("TestEvent")
                .putAttribute(TestEvent.TestBuilder.TEST_ATTRIBUTE, "Attribute")
                .assertHasAttributeValues(TestEvent.TestBuilder.TEST_ATTRIBUTE)
    }

    @Test(expected = AssertionError::class)
    fun testBuilderAssertHasAttributeValuesWithoutValues() {
        // Act:
        TestEvent.TestBuilder("TestEvent").assertHasAttributeValues(TestEvent.TestBuilder.TEST_ATTRIBUTE)
    }

    @Test fun testToString() {
        // Arrange:
        val event = TestEvent.TestBuilder("TestEvent")
                .destination("TEST")
                .putAttribute("ATTR.Text", "TestUsers")
                .putAttribute("ATTR.Count", 10)
                .build()
        // Act + Assert:
        assertThat(event.toString(), `is`("TestEvent{destinations: [TEST], attributes: {ATTR.Text=TestUsers, ATTR.Count=10}}"))
    }

    internal class TestEvent(builder: TestBuilder) : BaseAnalyticsEvent(builder) {

        class TestBuilder(eventType: String) : BaseAnalyticsEvent.BaseBuilder<TestBuilder, TestEvent>(eventType) {

            companion object {

                const val TEST_ATTRIBUTE = "ATTRIBUTE.Test"
            }

            override val self = this
            public override fun putAttribute(key: String, value: Serializable) = super.putAttribute(key, value)
            public override fun assertHasAttributeValues(vararg keys: String) = super.assertHasAttributeValues(*keys)
            override fun build() = TestEvent((this))
        }
    }
}