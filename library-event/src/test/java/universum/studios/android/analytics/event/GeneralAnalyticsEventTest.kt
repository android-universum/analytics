/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics.event

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`
import org.hamcrest.core.IsNot.not
import org.hamcrest.core.IsNull.notNullValue
import org.junit.Test
import universum.studios.android.test.TestCase
import java.io.Serializable

/**
 * @author Martin Albedinsky
 */
class GeneralAnalyticsEventTest : TestCase() {

    @Test fun testBuilder() {
        // Act:
        val builder = GeneralAnalyticsEvent.builder("TestEvent")
        // Assert:
        assertThat(builder, `is`(notNullValue()))
        assertThat(builder.eventType, `is`("TestEvent"))
        assertThat(builder, `is`(not(GeneralAnalyticsEvent.builder("TestEvent"))))
    }

    @Test fun testInstantiationWithoutProvidedArguments() {
        // Act:
        val event = GeneralAnalyticsEvent.builder("TestEvent").build()
        // Assert:
        assertThat(event, `is`(notNullValue()))
        assertThat(event.getType(), `is`("TestEvent"))
    }

    @Test fun testInstantiation() {
        // Act:
        val event = GeneralAnalyticsEvent.builder("TestEvent")
                .putAttribute("ATTR.Text", "TestUsers")
                .putAttribute("ATTR.Count", 10)
                .build()
        // Assert:
        assertThat(event, `is`(notNullValue()))
        assertThat(event.getType(), `is`("TestEvent"))
        assertThat(event.getDestinations().isEmpty(), `is`(true))
        val attributes = event.getAttributes()
        assertThat(attributes.size, `is`(2))
        assertThat(attributes["ATTR.Text"], `is`<Serializable>("TestUsers"))
        assertThat(attributes["ATTR.Count"], `is`<Serializable>(10))
    }

    @Test(expected = IllegalArgumentException::class)
    fun testInstantiationWithEmptyEventType() {
        // Act:
        GeneralAnalyticsEvent.builder("").build()
    }
}