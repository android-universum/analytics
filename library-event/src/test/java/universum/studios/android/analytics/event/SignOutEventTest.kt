/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics.event

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`
import org.hamcrest.core.IsNot.not
import org.hamcrest.core.IsNull.notNullValue
import org.junit.Test
import universum.studios.android.test.TestCase
import java.io.Serializable

/**
 * @author Martin Albedinsky
 */
class SignOutEventTest : TestCase() {

    @Test fun testType() {
        // Act + Assert:
        assertThat(SignOutEvent.TYPE, `is`("sign_out"))
    }

    @Test fun testAttributes() {
        // Act + Assert:
        assertThat(SignOutEvent.Attribute.SUCCESS, `is`("success"))
    }

    @Test fun testBuilder() {
        // Act:
        val builder = SignOutEvent.builder()
        // Assert:
        assertThat(builder, `is`(notNullValue()))
        assertThat(builder.eventType, `is`(SignOutEvent.TYPE))
        assertThat(builder, `is`(not(SignOutEvent.builder())))
    }

    @Test fun testInstantiation() {
        // Act:
        val event = SignOutEvent.builder().build()
        // Assert:
        assertThat(event, `is`(notNullValue()))
        assertThat(event.getType(), `is`(SignOutEvent.TYPE))
        assertThat(event.getDestinations().isEmpty(), `is`(true))
        val attributes = event.getAttributes()
        assertThat(attributes.size, `is`(1))
        assertThat(attributes[SignOutEvent.Attribute.SUCCESS], `is`<Serializable>(false))
    }

    @Test fun testInstantiationWithSuccess() {
        // Act:
        val event = SignOutEvent.builder().success(true).build()
        // Assert:
        assertThat(event, `is`(notNullValue()))
        assertThat(event.getType(), `is`(SignOutEvent.TYPE))
        assertThat(event.getDestinations().isEmpty(), `is`(true))
        val attributes = event.getAttributes()
        assertThat(attributes.size, `is`(1))
        assertThat(attributes[SignOutEvent.Attribute.SUCCESS], `is`<Serializable>(true))
    }
}