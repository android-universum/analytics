/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics.event

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`
import org.hamcrest.core.IsNot.not
import org.hamcrest.core.IsNull.notNullValue
import org.junit.Test
import universum.studios.android.test.TestCase
import java.io.Serializable

/**
 * @author Martin Albedinsky
 */
class SignInEventTest : TestCase() {

    @Test fun testType() {
        // Act + Assert:
        assertThat(SignInEvent.TYPE, `is`("sign_in"))
    }

    @Test fun testAttributes() {
        // Act + Assert:
        assertThat(SignInEvent.Attribute.METHOD, `is`("method"))
        assertThat(SignInEvent.Attribute.SUCCESS, `is`("success"))
    }

    @Test fun testMethods() {
        // Act:
        assertThat(SignInEvent.Method.PASSWORD, `is`("PASSWORD"))
    }

    @Test fun testBuilder() {
        // Act:
        val builder = SignInEvent.builder()
        // Assert:
        assertThat(builder, `is`(notNullValue()))
        assertThat(builder.eventType, `is`(SignInEvent.TYPE))
        assertThat(builder, `is`(not(SignInEvent.builder())))
    }

    @Test fun testInstantiation() {
        // Act:
        val event = SignInEvent.builder().method("UNKNOWN").build()
        // Assert:
        assertThat(event, `is`(notNullValue()))
        assertThat(event.getType(), `is`(SignInEvent.TYPE))
        assertThat(event.getDestinations().isEmpty(), `is`(true))
        val attributes = event.getAttributes()
        assertThat(attributes.size, `is`(2))
        assertThat(attributes[SignInEvent.Attribute.METHOD], `is`<Serializable>("UNKNOWN"))
        assertThat(attributes[SignInEvent.Attribute.SUCCESS], `is`<Serializable>(false))
    }

    @Test fun testInstantiationWithSuccess() {
        // Act:
        val event = SignInEvent.builder().method("UNKNOWN").success(true).build()
        // Assert:
        assertThat(event, `is`(notNullValue()))
        assertThat(event.getType(), `is`(SignInEvent.TYPE))
        assertThat(event.getDestinations().isEmpty(), `is`(true))
        val attributes = event.getAttributes()
        assertThat(attributes.size, `is`(2))
        assertThat(attributes[SignInEvent.Attribute.METHOD], `is`<Serializable>("UNKNOWN"))
        assertThat(attributes[SignInEvent.Attribute.SUCCESS], `is`<Serializable>(true))
    }

    @Test(expected = AssertionError::class)
    fun testInstantiationWithoutMethod() {
        // Act:
        SignInEvent.builder().build()
    }
}