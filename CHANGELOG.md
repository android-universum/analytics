Change-Log
===============
> Regular configuration update: _09.03.2020_

More **detailed changelog** for each respective version may be viewed by pressing on a desired _version's name_.

## Version 1.x ##

### [1.4.0](https://bitbucket.org/android-universum/analytics/wiki/version/1.x) ###
> 16.03.2020

- **This release bumps minimum requirements to Android 4.1+.**
- Resolved [Issue #10](https://bitbucket.org/android-universum/analytics/issues/10).
- Regular **dependencies update** (mainly artifacts from **Android Jetpack**).

### [1.3.1](https://bitbucket.org/android-universum/analytics/wiki/version/1.x) ###
> 07.12.2019

- Regular **dependencies update** (mainly artifacts from **Android Jetpack**).

### [1.3.0](https://bitbucket.org/android-universum/analytics/wiki/version/1.x) ###
> 25.10.2019

- Updated **event types** and **attribute names** to match _Firebase Analytics_ policies (lowercase string).

### [1.2.1](https://bitbucket.org/android-universum/analytics/wiki/version/1.x) ###
> 09.09.2019

- Updated dependencies to:
     - _Firebase_ **16.0.9**
     - _Crashlytics_ **2.10.1**
     - _Answers_ **1.4.7**

### [1.2.0](https://bitbucket.org/android-universum/analytics/wiki/version/1.x) ###
> 10.11.2018

- Regular **dependencies update** (mainly to use new artifacts from **Android Jetpack**).

### [1.1.1](https://bitbucket.org/android-universum/analytics/wiki/version/1.x) ###
> 02.11.2018

- Small code quality improvements.

### [1.1.0](https://bitbucket.org/android-universum/analytics/wiki/version/1.x) ###
> 23.07.2018

- Updated dependencies versions.

### [1.0.4](https://bitbucket.org/android-universum/analytics/wiki/version/1.x) ###
> 12.07.2018

- Small code quality improvements.

### [1.0.3](https://bitbucket.org/android-universum/analytics/wiki/version/1.x) ###
> 02.06.2018

- Added basic `Analytics` interface.

### [1.0.2](https://bitbucket.org/android-universum/analytics/wiki/version/1.x) ###
> 14.04.2018

- Updated **documentation** for the _code base_.

### [1.0.1](https://bitbucket.org/android-universum/analytics/wiki/version/1.x) ###
> 09.04.2018

- Removed `appplyChange(...)` function from all **builder** implementations.

### [1.0.0](https://bitbucket.org/android-universum/analytics/wiki/version/1.x) ###
> 25.03.2018

- First production release.
- `BaseAnalyticsEvent.Builder.addDestination(...)` replaced with `BaseAnalyticsEvent.Builder.destination(...)`
  and `BaseAnalyticsEvent.Builder.destinations(...)`.
- `ScreenViewAnalytics` replaced with `ScreenAnalytics` which is required to be instantiated via its builder.

## Version 0.x ##

### 0.9.0 ###
> 22.03.2018

- First pre-release.