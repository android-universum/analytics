Modules
===============

Library is also distributed via **separate modules** which may be downloaded as standalone parts of
the library in order to decrease dependencies count in Android projects, so only dependencies really
needed in an Android project are included. **However** some modules may depend on another modules
from this library or on modules from other libraries.

## Download ##

### Gradle ###

For **successful resolving** of artifacts for separate modules via **Gradle** add the following snippet
into **build.gradle** script of your desired Android project and use `implementation '...'` declaration
as usually.

    repositories {
        maven {
            url  "http://dl.bintray.com/universum-studios/android"
        }
    }

## Available modules ##
> Following modules are available in the [latest](https://bitbucket.org/android-universum/analytics/downloads "Downloads page") stable release.

- **[Core](https://bitbucket.org/android-universum/analytics/src/main/library-core)**
- **[Destination-Fabric](https://bitbucket.org/android-universum/analytics/src/main/library-destination-fabric)**
- **[Destination-Firebase](https://bitbucket.org/android-universum/analytics/src/main/library-destination-firebase)**
- **[Event](https://bitbucket.org/android-universum/analytics/src/main/library-event)**
- **[Screen](https://bitbucket.org/android-universum/analytics/src/main/library-screen)**
