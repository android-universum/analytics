Android Analytics
===============

[![CircleCI](https://circleci.com/bb/android-universum/analytics.svg?style=shield)](https://circleci.com/bb/android-universum/analytics)
[![Codecov](https://codecov.io/bb/android-universum/analytics/branch/main/graph/badge.svg)](https://codecov.io/bb/android-universum/analytics)
[![Codacy](https://api.codacy.com/project/badge/Grade/f62bf3adbc974270a094f7e26c6c1609)](https://www.codacy.com/app/universum-studios/analytics?utm_source=android-universum@bitbucket.org&amp;utm_medium=referral&amp;utm_content=android-universum/analytics&amp;utm_campaign=Badge_Grade)
[![Android](https://img.shields.io/badge/android-9.0-blue.svg)](https://developer.android.com/about/versions/pie/android-9.0)
[![Kotlin](https://img.shields.io/badge/kotlin-1.3.70-blue.svg)](https://kotlinlang.org)
[![Firebase](https://img.shields.io/badge/firebase--analytics-17.2.1-blue.svg)](https://firebase.google.com/products/crashlytics/)
[![Fabric](https://img.shields.io/badge/fabric--crashlytics-2.10.1-blue.svg)](https://fabric.io/kits/android/crashlytics/summary)
[![Robolectric](https://img.shields.io/badge/robolectric-4.3.1-blue.svg)](http://robolectric.org)
[![Android Jetpack](https://img.shields.io/badge/Android-Jetpack-brightgreen.svg)](https://developer.android.com/jetpack)

## ! OBSOLETE ! ##

**> This project has become obsolete and will be no longer maintained. <**

---

Analytics frameworks facade for the Android platform.

For more information please visit the **[Wiki](https://bitbucket.org/android-universum/analytics/wiki)**.

## Download ##

Download the latest **[release](https://bitbucket.org/android-universum/analytics/addon/pipelines/deployments "Deployments page")** or **add as dependency** in your project via:

### Gradle ###

    implementation "universum.studios.android:analytics:${DESIRED_VERSION}@aar"

## Modules ##

This library may be used via **separate [modules](https://bitbucket.org/android-universum/analytics/src/main/MODULES.md)**
in order to depend only on desired _parts of the library's code base_ what ultimately results in **fewer dependencies**.

## Compatibility ##

Supported down to the **Android [API Level 16](http://developer.android.com/about/versions/android-4.1.html "See API highlights")**.

### Dependencies ###

- [`androidx.annotation:annotation`](https://developer.android.com/jetpack/androidx)
- [`com.google.firebase:firebase-analytics`](https://firebase.google.com/docs/android/setup)
- [`com.crashlytics.sdk.android:answers`](https://fabric.io/kits/android/answers)
- [`com.crashlytics.sdk.android:crashlytics`](https://fabric.io/kits/android/crashlytics)
- [`universum.studios.android:logger`](https://bitbucket.org/android-universum/logger)

## [License](https://bitbucket.org/android-universum/analytics/src/main/LICENSE.md) ##

**Copyright 2020 Universum Studios**

_Licensed under the Apache License, Version 2.0 (the "License");_

You may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software distributed under the License
is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
or implied.
     
See the License for the specific language governing permissions and limitations under the License.