Analytics-Destination-Fabric
===============

This module contains implementation of analytics destination for the _[Fabric](https://fabric.io/home)_ tool.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aanalytics/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aanalytics/_latestVersion)

### Gradle ###

    compile "universum.studios.android:analytics-destination-fabric:${DESIRED_VERSION}@aar"

_depends on:_
[analytics-core](https://bitbucket.org/android-universum/analytics/src/main/library-core),
[analytics-event](https://bitbucket.org/android-universum/analytics/src/main/library-event)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [FabricDestination](https://bitbucket.org/android-universum/analytics/src/main/library-destination-fabric/src/main/java/universum/studios/android/analytics/FabricDestination.kt)
