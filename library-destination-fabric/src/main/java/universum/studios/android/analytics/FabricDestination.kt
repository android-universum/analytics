/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics

import android.util.Log
import androidx.annotation.NonNull
import androidx.annotation.VisibleForTesting
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent
import com.crashlytics.android.answers.LoginEvent
import universum.studios.android.analytics.event.SignInEvent

/**
 * An [AnalyticsDestination] implementation which dispatches all analytics events and failures passed
 * to either [logEvent] or [logFailure] to the [Crashlytics] instance (particularly to its [Answers])
 * the destination has been created with.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of FabircDestination with the given `crashlytics` instance.
 * @param crashlytics The desired Crashlytics instance which should be used by the new destination
 * for events logging.
 */
class FabricDestination private constructor(@NonNull private val crashlytics: Crashlytics) : AnalyticsDestination {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object {

        /**
         * Name of the Fabric analytics destination.
         */
        const val NAME = "FABRIC"

        /**
         * Logging tag.
         */
        internal const val TAG = "FabricDestination"

        /**
         * Creates a new instance of FabricDestination with the given [crashlytics] instance.
         *
         * @param crashlytics The desired Crashlytics instance which should be used by the new destination
         * for events logging.
         * @return Analytics destination ready to be used.
         */
        @JvmStatic @NonNull fun create(@NonNull crashlytics: Crashlytics) = FabricDestination(crashlytics)
    }

    /**
     * Class containing model mapper implementations used by this destination for mapping of analytics
     * event models specific for this analytics component to models that are specific for **Fabric API**.
     */
    @Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
    @VisibleForTesting internal class Mappers private constructor() {

        /**
         */
        companion object {

            /**
             * Mapper which maps [AnalyticsEvent] to [CustomEvent].
             */
            internal val TO_CUSTOM_EVENT = object : AnalyticsModelMapper<AnalyticsEvent, CustomEvent> {

                /*
                 */
                @NonNull override fun map(@NonNull event: AnalyticsEvent): CustomEvent {
                    val customEvent = CustomEvent(event.getType())
                    val attributes = event.getAttributes()
                    if (attributes.isNotEmpty()) {
                        attributes.forEach {
                            if (it.value is Number) {
                                customEvent.putCustomAttribute(it.key, it.value as Number)
                            } else {
                                customEvent.putCustomAttribute(it.key, it.value.toString())
                            }
                        }
                    }
                    return customEvent
                }
            }

            /**
             * Mapper which maps[AnalyticsEvent] to [LoginEvent].
             */
            internal val TO_LOGIN_EVENT = object : AnalyticsModelMapper<AnalyticsEvent, LoginEvent> {

                /*
                 */
                @NonNull override fun map(@NonNull event: AnalyticsEvent): LoginEvent {
                    val loginEvent = LoginEvent()
                    val attributes = event.getAttributes()
                    loginEvent.putMethod(attributes[SignInEvent.Attribute.METHOD].toString())
                    loginEvent.putSuccess(attributes[SignInEvent.Attribute.SUCCESS].toString().toBoolean())
                    return loginEvent
                }
            }

            /**
             * Mapper which maps [AnalyticsFailure] to [Throwable].
             */
            internal val FAILURE_TO_EXCEPTION = object : AnalyticsModelMapper<AnalyticsFailure, Throwable> {

                /*
                 */
                @NonNull override fun map(@NonNull failure: AnalyticsFailure): Throwable = failure.getCause()
            }
        }

        /**
         */
        init {
            // Not allowed to be instantiated publicly.
            throw UnsupportedOperationException()
        }
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /*
     * Initialization ==============================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /*
     */
    @NonNull override fun getName() = NAME

    /*
     */
    override fun logEvent(@NonNull event: AnalyticsEvent) {
        val answers = crashlytics.answers
        if (answers == null) {
            Log.w(TAG, "Answers not enabled. Skipping event logging.")
            return
        }
        when (event) {
            is SignInEvent -> answers.logLogin(Mappers.TO_LOGIN_EVENT.map(event))
            else -> answers.logCustom(Mappers.TO_CUSTOM_EVENT.map(event))
        }
    }

    /*
     */
    override fun logFailure(@NonNull failure: AnalyticsFailure) = crashlytics.core.logException(Mappers.FAILURE_TO_EXCEPTION.map(failure))

    /*
     * Inner classes ===============================================================================
     */
}