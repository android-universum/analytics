/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.analytics

import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent
import com.crashlytics.android.answers.LoginEvent
import com.crashlytics.android.beta.Beta
import com.crashlytics.android.core.CrashlyticsCore
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`
import org.hamcrest.core.IsNot.not
import org.hamcrest.core.IsNull.notNullValue
import org.junit.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoInteractions
import org.mockito.Mockito.verifyNoMoreInteractions
import universum.studios.android.analytics.event.GeneralAnalyticsEvent
import universum.studios.android.analytics.event.SignInEvent
import universum.studios.android.test.AndroidTestCase
import java.lang.reflect.InvocationTargetException

/**
 * @author Martin Albedinsky
 */
class FabricDestinationTest : AndroidTestCase() {

    @Test fun testName() {
        // Act + Assert:
        assertThat(FabricDestination.NAME, `is`("FABRIC"))
    }

    @Test(expected = IllegalAccessException::class)
    fun testMappersInstantiation() {
        // Act:
        FabricDestination.Mappers::class.java.newInstance()
    }

    @Test(expected = InvocationTargetException::class)
    fun testMappersInstantiationWithAccessibleConstructor() {
        // Act:
        FabricDestination.Mappers::class.java.getDeclaredConstructor().apply { isAccessible = true }.newInstance()
    }

    @Test fun testMappersToCustomEvent() {
        // Act:
        val customEvent = FabricDestination.Mappers.TO_CUSTOM_EVENT.map(GeneralAnalyticsEvent
                .builder("TestEvent")
                .putAttribute("ATTRIBUTE.Text", "Test")
                .putAttribute("ATTRIBUTE.Number", 10)
                .build()
        )
        // Assert:
        assertThat(customEvent, `is`(notNullValue()))
    }

    @Test fun testMappersToCustomEventWithoutAttributes() {
        // Act:
        val customEvent = FabricDestination.Mappers.TO_CUSTOM_EVENT.map(GeneralAnalyticsEvent.builder("TestEvent").build())
        // Assert:
        assertThat(customEvent, `is`(notNullValue()))
    }

    @Test fun testMappersToLoginEvent() {
        // Act:
        val loginEvent = FabricDestination.Mappers.TO_LOGIN_EVENT.map(SignInEvent.builder().method("PASS").build())
        // Assert:
        assertThat(loginEvent, `is`(notNullValue()))
    }

    @Test fun testMappersFailureToException() {
        // Arrange:
        val cause = IllegalStateException()
        // Act:
        val exception = FabricDestination.Mappers.FAILURE_TO_EXCEPTION.map(AnalyticsFailures.create(cause, "TEST"))
        // Assert:
        assertThat(exception, `is`(notNullValue()))
        assertThat(exception, `is`<Throwable>(cause))
    }

    @Test fun testCreate() {
        // Arrange:
        val mockCrashlytics = mock(Crashlytics::class.java)
        // Act:
        val destination = FabricDestination.create(mockCrashlytics)
        // Assert:
        assertThat(destination, `is`(notNullValue()))
        assertThat(destination.getName(), `is`(FabricDestination.NAME))
        assertThat(destination, `is`(not(FabricDestination.create(mockCrashlytics))))
        verifyNoInteractions(mockCrashlytics)
    }

    @Test fun testLogEvent() {
        // Arrange:
        val mockAnswers = mock(Answers::class.java)
        val crashlytics = createCrashlyticsWith(mockAnswers, mock(Beta::class.java), mock(CrashlyticsCore::class.java))
        val destination = FabricDestination.create(crashlytics)
        // Act + Assert:
        for (i in 1..10) {
            val mockEvent = mock(AnalyticsEvent::class.java)
            `when`(mockEvent.getType()).thenReturn("TestEvent")
            `when`(mockEvent.getAttributes()).thenReturn(emptyMap())
            destination.logEvent(mockEvent)
            verify(mockEvent).getAttributes()
        }
        verify(mockAnswers, times(10)).logCustom(any(CustomEvent::class.java))
    }

    @Test fun testLogEventSignIn() {
        // Arrange:
        val mockAnswers = mock(Answers::class.java)
        val crashlytics = createCrashlyticsWith(mockAnswers, mock(Beta::class.java), mock(CrashlyticsCore::class.java))
        val destination = FabricDestination.create(crashlytics)
        // Act:
        destination.logEvent(SignInEvent.builder().method(SignInEvent.Method.PASSWORD).build())
        // Assert:
        verify(mockAnswers).logLogin(any(LoginEvent::class.java))
        verifyNoMoreInteractions(mockAnswers)
    }

    @Test fun testLogEventToCrashlyticsWithoutAnswers() {
        // Arrange:
        val mockCrashlytics = mock(Crashlytics::class.java)
        val destination = FabricDestination.create(mockCrashlytics)
        // Act + Assert:
        for (i in 1..10) {
            val mockEvent = mock(AnalyticsEvent::class.java)
            destination.logEvent(mockEvent)
            verifyNoInteractions(mockEvent)
        }
    }

    @Test fun testLogFailure() {
        // Arrange:
        val mockCore = mock(CrashlyticsCore::class.java)
        val crashlytics = createCrashlyticsWith(mock(Answers::class.java), mock(Beta::class.java), mockCore)
        val destination = FabricDestination.create(crashlytics)
        val cause = IllegalStateException()
        // Act:
        destination.logFailure(AnalyticsFailures.create(cause, "TEST"))
        // Assert:
        verify(mockCore).logException(cause)
        verifyNoMoreInteractions(mockCore)
    }

    private fun createCrashlyticsWith(answers: Answers, beta: Beta, core: CrashlyticsCore): Crashlytics {
        val constructor = Crashlytics::class.java.getDeclaredConstructor(Answers::class.java, Beta::class.java, CrashlyticsCore::class.java)
        constructor.isAccessible = true
        return constructor.newInstance(answers, beta, core)
    }
}
